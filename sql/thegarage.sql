-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2016 at 05:31 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thegarage`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_floor`
--

CREATE TABLE IF NOT EXISTS `tbl_floor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `floor` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_floor`
--

INSERT INTO `tbl_floor` (`id`, `floor`, `status`, `updated_date`) VALUES
(1, 'first', 1, '2016-01-17 12:55:00'),
(2, 'Second', 1, '2016-01-18 02:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_floor_spots`
--

CREATE TABLE IF NOT EXISTS `tbl_floor_spots` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `floor_id` bigint(10) NOT NULL,
  `car_spots` int(5) NOT NULL,
  `mbike_spots` int(5) NOT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_floor_spots`
--

INSERT INTO `tbl_floor_spots` (`id`, `floor_id`, `car_spots`, `mbike_spots`, `updated_date`) VALUES
(1, 1, 14, 20, '2016-01-17 12:55:00'),
(2, 2, 10, 18, '2016-01-18 02:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_parking_spot_name`
--

CREATE TABLE IF NOT EXISTS `tbl_parking_spot_name` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `floor_id` int(5) NOT NULL,
  `spot_name` varchar(255) NOT NULL,
  `vehicle_type_id` bigint(10) NOT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `used` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `tbl_parking_spot_name`
--

INSERT INTO `tbl_parking_spot_name` (`id`, `floor_id`, `spot_name`, `vehicle_type_id`, `updated_date`, `used`, `status`) VALUES
(1, 1, '01CA01', 1, '2016-01-17 12:55:00', 0, 1),
(2, 1, '01CA02', 1, '2016-01-17 12:55:00', 1, 1),
(3, 1, '01CA03', 1, '2016-01-17 12:55:00', 0, 1),
(4, 1, '01CA04', 1, '2016-01-17 12:55:00', 0, 1),
(5, 1, '01CA05', 1, '2016-01-17 12:55:00', 1, 1),
(6, 1, '01CA06', 1, '2016-01-17 12:55:00', 0, 1),
(7, 1, '01CA07', 1, '2016-01-17 12:55:00', 0, 1),
(8, 1, '01CA08', 1, '2016-01-17 12:55:00', 1, 1),
(9, 1, '01CA09', 1, '2016-01-17 12:55:00', 0, 1),
(10, 1, '01CA10', 1, '2016-01-17 12:55:00', 1, 1),
(11, 1, '01CA11', 1, '2016-01-17 12:55:00', 0, 1),
(12, 1, '01CA12', 1, '2016-01-17 12:55:00', 1, 1),
(13, 1, '01CA13', 1, '2016-01-17 12:55:00', 1, 1),
(14, 1, '01CA14', 1, '2016-01-17 12:55:00', 0, 1),
(15, 1, '01MB01', 2, '2016-01-17 12:55:00', 0, 1),
(16, 1, '01MB02', 2, '2016-01-17 12:55:00', 1, 1),
(17, 1, '01MB03', 2, '2016-01-17 12:55:00', 0, 1),
(18, 1, '01MB04', 2, '2016-01-17 12:55:00', 0, 1),
(19, 1, '01MB05', 2, '2016-01-17 12:55:00', 0, 1),
(20, 1, '01MB06', 2, '2016-01-17 12:55:00', 0, 1),
(21, 1, '01MB07', 2, '2016-01-17 12:55:00', 0, 1),
(22, 1, '01MB08', 2, '2016-01-17 12:55:00', 0, 1),
(23, 1, '01MB09', 2, '2016-01-17 12:55:00', 0, 1),
(24, 1, '01MB10', 2, '2016-01-17 12:55:00', 0, 1),
(25, 1, '01MB11', 2, '2016-01-17 12:55:00', 0, 1),
(26, 1, '01MB12', 2, '2016-01-17 12:55:00', 1, 1),
(27, 1, '01MB13', 2, '2016-01-17 12:55:00', 0, 1),
(28, 1, '01MB14', 2, '2016-01-17 12:55:00', 0, 1),
(29, 1, '01MB15', 2, '2016-01-17 12:55:00', 0, 1),
(30, 1, '01MB16', 2, '2016-01-17 12:55:00', 0, 1),
(31, 1, '01MB17', 2, '2016-01-17 12:55:00', 0, 1),
(32, 1, '01MB18', 2, '2016-01-17 12:55:00', 0, 1),
(33, 1, '01MB19', 2, '2016-01-17 12:55:00', 0, 1),
(34, 1, '01MB20', 2, '2016-01-17 12:55:00', 1, 1),
(35, 2, '02CA01', 1, '2016-01-18 02:14:17', 0, 1),
(36, 2, '02CA02', 1, '2016-01-18 02:14:17', 0, 1),
(37, 2, '02CA03', 1, '2016-01-18 02:14:17', 0, 1),
(38, 2, '02CA04', 1, '2016-01-18 02:14:17', 0, 1),
(39, 2, '02CA05', 1, '2016-01-18 02:14:17', 0, 1),
(40, 2, '02CA06', 1, '2016-01-18 02:14:17', 0, 1),
(41, 2, '02CA07', 1, '2016-01-18 02:14:17', 0, 1),
(42, 2, '02CA08', 1, '2016-01-18 02:14:17', 0, 1),
(43, 2, '02CA09', 1, '2016-01-18 02:14:17', 0, 1),
(44, 2, '02CA10', 1, '2016-01-18 02:14:17', 1, 1),
(45, 2, '02MB01', 2, '2016-01-18 02:14:17', 0, 1),
(46, 2, '02MB02', 2, '2016-01-18 02:14:17', 0, 1),
(47, 2, '02MB03', 2, '2016-01-18 02:14:17', 0, 1),
(48, 2, '02MB04', 2, '2016-01-18 02:14:17', 0, 1),
(49, 2, '02MB05', 2, '2016-01-18 02:14:17', 0, 1),
(50, 2, '02MB06', 2, '2016-01-18 02:14:17', 0, 1),
(51, 2, '02MB07', 2, '2016-01-18 02:14:17', 0, 1),
(52, 2, '02MB08', 2, '2016-01-18 02:14:17', 1, 1),
(53, 2, '02MB09', 2, '2016-01-18 02:14:17', 0, 1),
(54, 2, '02MB10', 2, '2016-01-18 02:14:17', 0, 1),
(55, 2, '02MB11', 2, '2016-01-18 02:14:17', 0, 1),
(56, 2, '02MB12', 2, '2016-01-18 02:14:17', 0, 1),
(57, 2, '02MB13', 2, '2016-01-18 02:14:17', 0, 1),
(58, 2, '02MB14', 2, '2016-01-18 02:14:17', 0, 1),
(59, 2, '02MB15', 2, '2016-01-18 02:14:17', 0, 1),
(60, 2, '02MB16', 2, '2016-01-18 02:14:17', 0, 1),
(61, 2, '02MB17', 2, '2016-01-18 02:14:17', 1, 1),
(62, 2, '02MB18', 2, '2016-01-18 02:14:17', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_used_spots`
--

CREATE TABLE IF NOT EXISTS `tbl_used_spots` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `vehicle_identifier` text NOT NULL,
  `parking_spot_id` bigint(10) NOT NULL,
  `vehicle_type_id` bigint(10) NOT NULL,
  `active` int(1) NOT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbl_used_spots`
--

INSERT INTO `tbl_used_spots` (`id`, `vehicle_identifier`, `parking_spot_id`, `vehicle_type_id`, `active`, `updated_date`) VALUES
(1, 'DV001', 1, 1, 0, '0000-00-00 00:00:00'),
(2, 'XYZ999', 4, 1, 0, '0000-00-00 00:00:00'),
(3, 'AB0564', 20, 2, 0, '0000-00-00 00:00:00'),
(4, 'OPO210', 12, 1, 1, '0000-00-00 00:00:00'),
(5, 'BHQ123', 5, 1, 1, '0000-00-00 00:00:00'),
(6, 'QWE-980', 8, 1, 1, '0000-00-00 00:00:00'),
(7, 'tricc11', 14, 1, 0, '0000-00-00 00:00:00'),
(8, 'POP246', 22, 2, 0, '2016-01-17 22:45:52'),
(9, 'XYZ-111', 14, 1, 0, '2016-01-17 23:07:49'),
(10, 'OPO-111', 7, 1, 0, '2016-01-17 23:12:55'),
(11, 'PLOY-052', 3, 1, 0, '2016-01-17 23:13:20'),
(12, 'OWLS', 2, 1, 1, '2016-01-18 00:50:59'),
(13, 'PUPPY', 14, 1, 0, '2016-01-18 03:39:16'),
(14, 'DUCKY', 13, 1, 1, '2016-01-18 03:39:26'),
(15, 'POPPED', 1, 1, 0, '2016-01-18 03:39:39'),
(16, 'YOTA', 10, 1, 1, '2016-01-18 03:39:49'),
(17, 'LIVELY', 4, 1, 0, '2016-01-18 03:40:08'),
(18, 'PLUG777', 44, 1, 1, '2016-01-18 03:40:36'),
(19, 'BA41PA3967', 53, 2, 0, '2016-01-18 04:07:06'),
(20, 'DC-0011P', 52, 2, 1, '2016-01-18 04:13:18'),
(21, 'PLOG-111', 16, 2, 1, '2016-01-18 04:15:02'),
(22, 'LOOP789', 34, 2, 1, '2016-01-18 04:40:16'),
(23, 'MOBO111', 61, 2, 1, '2016-01-18 04:41:50'),
(24, 'TRST', 26, 2, 1, '2016-01-18 04:44:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `role` varchar(255) NOT NULL,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `username`, `password`, `role`, `updated_date`, `status`) VALUES
(1, 'Administrator', 'admin', 'e9fd363bedc114628fe2cdce1493db15', 'ADMIN_ROLE', '2016-01-16 22:42:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vehicle_types`
--

CREATE TABLE IF NOT EXISTS `tbl_vehicle_types` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `vehicle_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_vehicle_types`
--

INSERT INTO `tbl_vehicle_types` (`id`, `vehicle_type`) VALUES
(1, 'Car'),
(2, 'Motorbike');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
