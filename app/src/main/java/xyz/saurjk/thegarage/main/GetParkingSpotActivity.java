package xyz.saurjk.thegarage.main;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.db.DatabaseDataSource;
import xyz.saurjk.thegarage.dto.ParkingSpotDTO;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class GetParkingSpotActivity extends AppCompatActivity {

    private static final String TAG = "GetParkingSpotActivity";
    private static final boolean debug = true;
    private static final String TAG_RESULT = "result";
    private static final String TAG_PARKING_SPOT = "assigned_spot";
    private static final String TAG_FLOOR = "floorname";

    private DatabaseDataSource mDatabaseDataSource;

    EditText licensePlate;
    RadioGroup radioVehicleGroup;

    Button getParkingSpotButton;
    RadioButton radioCar, radioMBike;
    RadioButton selectedRadio;

    String selectedVehicle;
    String assignedParkingSpot;
    String licensePlateValue;
    String floorName;

    int result;
    int selectedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_parking_spot);

        licensePlate = (EditText) findViewById(R.id.licensePlate);
        radioVehicleGroup = (RadioGroup) findViewById(R.id.radioVehicle);
        radioCar = (RadioButton) findViewById(R.id.radioCar);
        radioMBike = (RadioButton) findViewById(R.id.radioMBike);

        mDatabaseDataSource = new DatabaseDataSource(this);
        getParkingSpotButton = (Button) findViewById(R.id.getParkingSpotButton);

        getParkingSpotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    licensePlateValue = licensePlate.getText().toString();
                    if (licensePlateValue.equalsIgnoreCase(null) || licensePlateValue.isEmpty() || licensePlateValue.equalsIgnoreCase("")) {
                        throw new NullPointerException();
                    }
                    selectedId = radioVehicleGroup.getCheckedRadioButtonId();
                    if (debug)
                        Log.d(TAG, "License Plate Value: " + licensePlateValue + ", SelectedId: " + selectedId);

                    selectedRadio = (RadioButton) findViewById(selectedId);
                    selectedVehicle = selectedRadio.getText().toString();
                    if (debug) Log.d(TAG, "Selected Radio: " + selectedRadio.getText());

                    new GetParkingSpot().execute();

                } catch (NullPointerException npe) {
                    Toast.makeText(getApplicationContext(), "Please enter your license plates and/or select a vehicle type.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Methods to connected Android application with PHP API
     */
    class GetParkingSpot extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String urlString = Constants.URL_STRING + "assign_parking_spot";
            URL url = null;
            try {
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                StringBuffer paramsBuilder = new StringBuffer();
                paramsBuilder.append("vehicle_identifier=");
                paramsBuilder.append(licensePlateValue);
                paramsBuilder.append("&vehicle_type=");
                paramsBuilder.append(selectedVehicle);

                PrintWriter requestWriter = new PrintWriter(httpURLConnection.getOutputStream(), true);
                requestWriter.print(paramsBuilder.toString());
                requestWriter.close();

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                String response = Conversion.inputStreamToString(in).toString();

                JSONObject jsonObject = new JSONObject(response);
                result = jsonObject.getInt(TAG_RESULT);
                if (result == 1) {
                    assignedParkingSpot = jsonObject.getString(TAG_PARKING_SPOT);
                    floorName = jsonObject.getString(TAG_FLOOR);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (result) {
                case 0:
                    Toast.makeText(getApplicationContext(), "All " + selectedVehicle + " Parking Spots Taken.", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    try {
                        mDatabaseDataSource.open();
                        ParkingSpotDTO parkingSpotDTO = new ParkingSpotDTO();
                        parkingSpotDTO.setVehicleId(licensePlateValue);
                        parkingSpotDTO.setParkingSpot(assignedParkingSpot);
                        parkingSpotDTO.setActive(new Integer("1"));
                        mDatabaseDataSource.insertParkingSpotData(parkingSpotDTO);
                        mDatabaseDataSource.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    Intent assignSpotIntent = new Intent(GetParkingSpotActivity.this, AssignedParkingSpotActivity.class);
                    assignSpotIntent.putExtra("parkingSpot", assignedParkingSpot);
                    assignSpotIntent.putExtra("vehicleId", licensePlateValue);
                    assignSpotIntent.putExtra("floorName", floorName);
                    finish();
                    startActivity(assignSpotIntent);
                    break;
                case 2:
                    Toast.makeText(getApplicationContext(), "The Vehicle is already inside the parking lot.", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
