package xyz.saurjk.thegarage.main;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.admin.MemberLoginActivity;
import xyz.saurjk.thegarage.db.DatabaseDataSource;
import xyz.saurjk.thegarage.dto.ParkingSpotDTO;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";
    private static final boolean debug = true;
    private static final String TAG_RESULT = "result";
    private static final String TAG_UNUSED_CAR = "unused_car_spots";
    private static final String TAG_UNUSED_MBIKE = "unused_mbike_spots";

    private boolean doubleBackToExitPressedOnce = false;
    Button getParkingSpotButton;
    Button memberLoginButton;
    Button exitGarageButton;

    DatabaseDataSource mDatabaseDataSource;

    RelativeLayout parkedListRelativeLayout;
    TextView parkedVehiclesLabel;
    List<ParkingSpotDTO> parkingSpotDTOList;
    Spinner parkingSpotSpinner;

    ArrayAdapter<String> spinnerAdapter;
    ArrayList<String> parkingArrayList;

    String vehicleId;
    String parkingSpot;
    String unusedCarSpots;
    String unusedMbikeSpots;

    int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getParkingSpotButton = (Button) findViewById(R.id.getParkingSpotButton);
        memberLoginButton = (Button) findViewById(R.id.memberLogin);

        parkedListRelativeLayout = (RelativeLayout) findViewById(R.id.parkedVehiclesLayout);
        parkedVehiclesLabel = (TextView) findViewById(R.id.parkedVehiclesLabel);
        parkingSpotSpinner = (Spinner) findViewById(R.id.parkingSpotSpinner);
        exitGarageButton = (Button) findViewById(R.id.exitGarageButton);

        parkedListRelativeLayout.setVisibility(View.INVISIBLE);
        parkedVehiclesLabel.setVisibility(View.INVISIBLE);
        parkingSpotSpinner.setVisibility(View.INVISIBLE);
        exitGarageButton.setVisibility(View.INVISIBLE);

        generateParkingSpotSpinner();

        getParkingSpotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGetParkingSpot();
            }
        });
        memberLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMemberLogin();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetFreeSpots().execute();
    }

    /***
     * Method to generate the spinner if the user has parked a vehicle in the garage.
     */
    private void generateParkingSpotSpinner() {
        mDatabaseDataSource = new DatabaseDataSource(this);
        try{
            mDatabaseDataSource.open();
            parkingSpotDTOList = mDatabaseDataSource.readParkingSpotData();
            mDatabaseDataSource.close();
            if(parkingSpotDTOList.size()>0){
                parkingArrayList = new ArrayList<String>();
                for(ParkingSpotDTO parkingSpotDTO : parkingSpotDTOList){
                    parkingArrayList.add(parkingSpotDTO.getVehicleId() + " - " + parkingSpotDTO.getParkingSpot());
                }
                Log.d(TAG, "Size > 0");

                spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, parkingArrayList);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                parkingSpotSpinner.setAdapter(spinnerAdapter);

                parkedListRelativeLayout.setVisibility(View.VISIBLE);
                parkedVehiclesLabel.setVisibility(View.VISIBLE);
                parkingSpotSpinner.setVisibility(View.VISIBLE);
                exitGarageButton.setVisibility(View.VISIBLE);

                exitGarageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = parkingSpotSpinner.getSelectedItemPosition();
                        vehicleId = parkingSpotDTOList.get(position).getVehicleId();
                        parkingSpot = parkingSpotDTOList.get(position).getParkingSpot();
                        try {
                            mDatabaseDataSource.open();
                            mDatabaseDataSource.exitGarage(vehicleId);
                            mDatabaseDataSource.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        new ExitGarage().execute();
                        new GetFreeSpots().execute();
                    }
                });
                Log.d(TAG, "Parked");
            }else {
                parkedListRelativeLayout.setVisibility(View.INVISIBLE);
                parkedVehiclesLabel.setVisibility(View.INVISIBLE);
                parkingSpotSpinner.setVisibility(View.INVISIBLE);
                exitGarageButton.setVisibility(View.INVISIBLE);
                Log.d(TAG, "Not Parked");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void goToMemberLogin() {
        Intent goToMemberLoginIntent = new Intent(HomeActivity.this, MemberLoginActivity.class);
        startActivity(goToMemberLoginIntent);
    }

    private void goToGetParkingSpot(){
        Intent parkingSpotIntent = new Intent(HomeActivity.this, GetParkingSpotActivity.class);
        startActivity(parkingSpotIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Exit the app if back button is double tapped
     */
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * Methods to connected Android application with PHP API
     */
    class ExitGarage extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {
            String urlString = Constants.URL_STRING + "exit_garage";
            URL url = null;
            try {
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                StringBuffer paramsBuilder = new StringBuffer();
                paramsBuilder.append("vehicle_identifier=");
                paramsBuilder.append(vehicleId);
                paramsBuilder.append("&parking_spot=");
                paramsBuilder.append(parkingSpot);

                PrintWriter requestWriter = new PrintWriter(httpURLConnection.getOutputStream(), true);
                requestWriter.print(paramsBuilder.toString());
                requestWriter.close();

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

                String response = Conversion.inputStreamToString(in).toString();

                JSONObject jsonObject = new JSONObject(response);
                result = jsonObject.getInt(TAG_RESULT);
            }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getApplicationContext(), "Exited Successfully.", Toast.LENGTH_SHORT).show();
            generateParkingSpotSpinner();
        }
    }

    class GetFreeSpots extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, "Inside CheckFreeSpots");
            String urlString = Constants.URL_STRING + "get_free_parking_spots";
            URL url = null;
            try{
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                String response = Conversion.inputStreamToString(in).toString();

                JSONObject jsonObject = new JSONObject(response);
                unusedCarSpots = jsonObject.getString(TAG_UNUSED_CAR);
                unusedMbikeSpots = jsonObject.getString(TAG_UNUSED_MBIKE);
            }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            TextView freeSpotsTV = (TextView) findViewById(R.id.freeSpotsTV);
            String freeSpotsMessage = "Free Parking Spots : Cars - " + unusedCarSpots + ", Motorbikes - " + unusedMbikeSpots;
            freeSpotsTV.setText(freeSpotsMessage);
        }
    }
}
