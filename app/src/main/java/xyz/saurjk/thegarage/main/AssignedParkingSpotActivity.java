package xyz.saurjk.thegarage.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import xyz.saurjk.thegarage.R;

public class AssignedParkingSpotActivity extends AppCompatActivity {

    TextView assignedSpotTV;
    TextView floorTV;

    String parkingSpot;
    String vehicleId;
    String floorName;

    Button homeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_parking_spot);

        assignedSpotTV = (TextView) findViewById(R.id.assignedSpot);
        floorTV = (TextView) findViewById(R.id.floorTV);
        homeButton = (Button) findViewById(R.id.homeButton);

        Intent intent = getIntent();
        if(null != intent){
            parkingSpot = intent.getStringExtra("parkingSpot");
            vehicleId = intent.getStringExtra("vehicleId");
            floorName= intent.getStringExtra("floorName");
            assignedSpotTV.setText(parkingSpot);
            floorTV.setText(floorName);
        }

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent goHomeIntent = new Intent(AssignedParkingSpotActivity.this, HomeActivity.class);
                startActivity(goHomeIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
