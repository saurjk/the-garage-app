package xyz.saurjk.thegarage.admin;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class AddNewFloorPlan extends AppCompatActivity {

    private static final String TAG = "AddNewFloorPlanActivity";
    private static final boolean debug = true;

    EditText floorValueET;
    EditText carSpotsValueET;
    EditText mBikeSpotsValueET;

    Button addFloorButton;

    TextView numLevelsValTV;

    String floorValue;

    int carSpotsValue;
    int mBikeSpotsValue;
    int result;

    private static final String TAG_RESULT = "result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_floor_plan);

        floorValueET = (EditText) findViewById(R.id.floorValue);
        carSpotsValueET = (EditText) findViewById(R.id.carSpotsValue);
        mBikeSpotsValueET = (EditText) findViewById(R.id.mbikeSpotsValue);
        addFloorButton = (Button) findViewById(R.id.addFloorButton);
        numLevelsValTV = (TextView) findViewById(R.id.numLevelsVal);

        Intent intent = getIntent();
        if(null != intent){
            numLevelsValTV.setText(intent.getStringExtra("totalNumFloors"));
        }

        addFloorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    floorValue = floorValueET.getText().toString();
                    if(floorValue.isEmpty()){
                        throw new NullPointerException();
                    }
                    carSpotsValue = Integer.parseInt(carSpotsValueET.getText().toString());
                    mBikeSpotsValue = Integer.parseInt(mBikeSpotsValueET.getText().toString());
                    new AddNewFloorPlanASync().execute();
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(), "Please Enter all values.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * Methods to connected Android application with PHP API
     */
    class AddNewFloorPlanASync extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {
            String urlString = Constants.URL_STRING + "add_floor_plan";
            URL url = null;
            try{
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                StringBuffer paramsBuilder = new StringBuffer();
                paramsBuilder.append("floor=");
                paramsBuilder.append(floorValue);
                paramsBuilder.append("&carspotsvalue=");
                paramsBuilder.append(carSpotsValue);
                paramsBuilder.append("&mbike_spots_val=");
                paramsBuilder.append(mBikeSpotsValue);

                PrintWriter requestWriter = new PrintWriter(httpURLConnection.getOutputStream(), true);
                requestWriter.print(paramsBuilder.toString());
                requestWriter.close();

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                String response = Conversion.inputStreamToString(in).toString();
                JSONObject jsonObject = new JSONObject(response);
                result = jsonObject.getInt(TAG_RESULT);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (result){
                case 0:
                    Toast.makeText(getApplicationContext(), "Something went wrong.", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(getApplicationContext(), "New Floor Added.", Toast.LENGTH_SHORT).show();
                    finish();
                    Intent backToAdminIntent = new Intent(AddNewFloorPlan.this, AdminActivity.class);
                    startActivity(backToAdminIntent);
                    break;
                case 2:
                    Toast.makeText(getApplicationContext(), "Floor Already Present.", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
