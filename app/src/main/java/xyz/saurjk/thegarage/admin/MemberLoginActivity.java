package xyz.saurjk.thegarage.admin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.main.HomeActivity;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class MemberLoginActivity extends AppCompatActivity {

    private static final String TAG = "MemberLoginActivity";
    private static final boolean debug = true;

    private static final String TAG_USERNAME = "member_username";
    private static final String TAG_PASSWORD = "member_password";
    private static final String TAG_NAME = "member_name";
    private static final String TAG_ROLE = "tag_role";
    private static final String TAG_STATUS = "tag_status";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERDATA = "userdata";

    EditText memberUsernameET;
    EditText memberUserPasswordET;

    Button memberLoginButton;
    Button buttonHome;

    String memberUsername;
    String memberPasswordEntered;
    String memberPasswordFromDB;
    String memberName;
    String memberRole;
    String memberStatus;
    int success;
    boolean loggedIn = false;

    public static final String MyPreferences = "MyPrefs";
    public static final String UsernameKey = "UsernameKey";
    public static final String NameKey = "NameKey";
    public static final String RoleKey = "RoleKey";
    public static final String LoggedKey = "LoggedKey";
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_login);

        sharedPreferences = getSharedPreferences(MyPreferences, Context.MODE_PRIVATE);
        boolean loggedFromPrefs = sharedPreferences.getBoolean(LoggedKey, false);

        if(loggedFromPrefs){
            Intent adminLoggedIntent = new Intent(MemberLoginActivity.this, AdminActivity.class);
            startActivity(adminLoggedIntent);
        }
        memberUsernameET = (EditText) findViewById(R.id.memberUsername);
        memberUserPasswordET = (EditText) findViewById(R.id.memberPassword);
        memberLoginButton = (Button) findViewById(R.id.memberLoginButton);
        buttonHome = (Button) findViewById(R.id.buttonHome);

        memberLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memberUsername = memberUsernameET.getText().toString();
                if(memberUsername.isEmpty() || memberUsername.equalsIgnoreCase("") || memberUsername.equalsIgnoreCase(null)){
                    Toast.makeText(getApplicationContext(), "Please enter your username.", Toast.LENGTH_SHORT).show();
                }
                memberPasswordEntered = Conversion.convertStringToSHA1AndMD5(memberUserPasswordET.getText().toString());
                if(memberPasswordEntered.isEmpty() || memberPasswordEntered.equalsIgnoreCase("") || memberPasswordEntered.equalsIgnoreCase(null)){
                    Toast.makeText(getApplicationContext(), "Please enter your password.", Toast.LENGTH_SHORT).show();
                }
                new MemberLogin().execute();
            }
        });

        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent(MemberLoginActivity.this, HomeActivity.class);
                startActivity(homeIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean loggedFromPrefs = sharedPreferences.getBoolean(LoggedKey, false);

        if(loggedFromPrefs){
            Intent adminLoggedIntent = new Intent(MemberLoginActivity.this, AdminActivity.class);
            startActivity(adminLoggedIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent homeIntent = new Intent(MemberLoginActivity.this, HomeActivity.class);
        startActivity(homeIntent);
    }

    /**
     * Methods to connected Android application with PHP API
     */
    class MemberLogin extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params){
            String urlString = Constants.URL_STRING + "user_login";
            URL url = null;
            try{
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                StringBuffer paramsBuilder = new StringBuffer();
                paramsBuilder.append("username=");
                paramsBuilder.append(memberUsername);

                PrintWriter requestWriter = new PrintWriter(httpURLConnection.getOutputStream(), true);
                requestWriter.print(paramsBuilder.toString());
                requestWriter.close();

                if(debug) Log.d(TAG, "Response Code : " + httpURLConnection.getResponseCode());
                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

                String response = Conversion.inputStreamToString(in).toString();
                JSONObject jsonObject = new JSONObject(response);
                success = jsonObject.getInt(TAG_SUCCESS);
                if(success == 1){
                    JSONArray jsonArray = jsonObject.getJSONArray(TAG_USERDATA);
                    JSONObject userDataJSONObject = jsonArray.getJSONObject(0);
                    memberPasswordFromDB = userDataJSONObject.getString(TAG_PASSWORD);

                    if(memberPasswordFromDB.equalsIgnoreCase(memberPasswordEntered)){
                        loggedIn = true;

                        memberName = userDataJSONObject.getString(TAG_NAME);
                        memberRole = userDataJSONObject.getString(TAG_ROLE);
                        memberStatus = userDataJSONObject.getString(TAG_STATUS);
                    }else{
                        loggedIn = false;
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s){
            if(success == 1) {
                if(loggedIn) {
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putString(UsernameKey, memberUsername);
                    editor.putString(NameKey, memberName);
                    editor.putBoolean(LoggedKey, loggedIn);
                    editor.putString(RoleKey, memberRole);
                    editor.commit();

                    Log.d(TAG, "Logged In");
                    Toast.makeText(getApplicationContext(), "Login Successful.", Toast.LENGTH_SHORT).show();
                    Intent adminLoggedIntent = new Intent(MemberLoginActivity.this, AdminActivity.class);
                    startActivity(adminLoggedIntent);
                }else{
                    Log.d(TAG, "Not Logged In");
                    Toast.makeText(getApplicationContext(), "Login Failed. Incorrect Username and/or Password.", Toast.LENGTH_SHORT).show();
                }
            }else{
                Log.d(TAG, "User Not Present");
                Toast.makeText(getApplicationContext(), "User not found.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
