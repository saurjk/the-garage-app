package xyz.saurjk.thegarage.admin;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.main.HomeActivity;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class AdminActivity extends AppCompatActivity {

    private static final String TAG = "AdminActivity";

    Button logoutButton;
    Button addFloorButton;
    Button freeAvailableButton;
    Button findSpecificVehicleButton;
    Button addUserButton;
    SharedPreferences sharedPreferences;

    String totalNumFloors;

    private static final String TAG_TOTAL_FLOORS = "existing_floors";
    private static final String TAG_UNUSED_CAR = "unused_car_spots";
    private static final String TAG_UNUSED_MBIKE = "unused_mbike_spots";

    String unusedCarSpots;
    String unusedMbikeSpots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        sharedPreferences = getSharedPreferences(MemberLoginActivity.MyPreferences, Context.MODE_PRIVATE);

        logoutButton = (Button) findViewById(R.id.logoutButton);
        addFloorButton = (Button) findViewById(R.id.addFloorButton);
        freeAvailableButton = (Button) findViewById(R.id.freeAvailableButton);
        findSpecificVehicleButton = (Button) findViewById(R.id.findSpecificVehicleButton);
        addUserButton = (Button) findViewById(R.id.addUserButton);

        addUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addUserIntent = new Intent(AdminActivity.this, CreateUserActivity.class);
                startActivity(addUserIntent);
            }
        });

        addFloorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TotalLevels().execute();
            }
        });

        findSpecificVehicleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent findSpecificVehicleIntent = new Intent(AdminActivity.this, QueryCarActivity.class);
                startActivity(findSpecificVehicleIntent);
            }
        });

        freeAvailableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CheckFreeSpots().execute();
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(MemberLoginActivity.LoggedKey);
                editor.remove(MemberLoginActivity.RoleKey);
                editor.remove(MemberLoginActivity.NameKey);
                editor.remove(MemberLoginActivity.UsernameKey);
                editor.clear();
                editor.commit();
                finish();

                Intent notLoggedIntent = new Intent(AdminActivity.this, MemberLoginActivity.class);
                startActivity(notLoggedIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean loggedIn = sharedPreferences.getBoolean(MemberLoginActivity.LoggedKey, false);
        if(!loggedIn){
            Intent notLoggedIntent = new Intent(AdminActivity.this, MemberLoginActivity.class);
            startActivity(notLoggedIntent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        boolean loggedIn = sharedPreferences.getBoolean(MemberLoginActivity.LoggedKey, false);
        if(loggedIn){
            Intent backPressIntent = new Intent(AdminActivity.this, HomeActivity.class);
            startActivity(backPressIntent);
        }
    }

    /**
     * Methods to connected Android application with PHP API
     */
    class TotalLevels extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String urlString = Constants.URL_STRING + "get_floors_existing";
            URL url = null;
            try{
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                String response = Conversion.inputStreamToString(in).toString();

                JSONObject jsonObject = new JSONObject(response);
                totalNumFloors = jsonObject.getString(TAG_TOTAL_FLOORS);
            }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Intent addFloorIntent = new Intent(AdminActivity.this, AddNewFloorPlan.class);
            addFloorIntent.putExtra("totalNumFloors", totalNumFloors);
            startActivity(addFloorIntent);
        }
    }

    class CheckFreeSpots extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.d(TAG, "Inside CheckFreeSpots");
            String urlString = Constants.URL_STRING + "get_free_parking_spots";
            URL url = null;
            try{
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");

                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                String response = Conversion.inputStreamToString(in).toString();

                JSONObject jsonObject = new JSONObject(response);
                unusedCarSpots = jsonObject.getString(TAG_UNUSED_CAR);
                unusedMbikeSpots = jsonObject.getString(TAG_UNUSED_MBIKE);
            }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            final Dialog dialog = new Dialog(AdminActivity.this);
            dialog.setContentView(R.layout.custom_dialog);

            TextView freeCarSpots = (TextView) dialog.findViewById(R.id.unusedSpotsCar);
            freeCarSpots.setText("Car : " + unusedCarSpots);

            TextView freeMBikeSpots = (TextView) dialog.findViewById(R.id.unusedSportMbike);
            freeMBikeSpots.setText("Motorbike : " + unusedMbikeSpots);

            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
}
