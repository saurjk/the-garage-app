package xyz.saurjk.thegarage.admin;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class CreateUserActivity extends AppCompatActivity {

    private static final String TAG = "CreateUserActivity";
    private static final boolean debug = false;
    private static final String TAG_RESULT = "result";

    EditText nameET;
    EditText usernameET;
    EditText passwordET;
    EditText rePasswordET;

    Button addUserButton;

    String name;
    String username;
    String password;
    String rePassword;
    String convertedPassword;
    boolean result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        nameET = (EditText) findViewById(R.id.nameEditText);
        usernameET = (EditText) findViewById(R.id.usernameET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        rePasswordET = (EditText) findViewById(R.id.rePasswordET);
        addUserButton = (Button) findViewById(R.id.addUserButton);

        addUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    name = nameET.getText().toString();
                    username = usernameET.getText().toString();
                    password = passwordET.getText().toString();
                    rePassword = rePasswordET.getText().toString();

                    if(name.isEmpty()||name.equalsIgnoreCase("")||name.equalsIgnoreCase(null)){
                        throw new NullPointerException();
                    }
                    if(username.isEmpty()||username.equalsIgnoreCase("")||username.equalsIgnoreCase(null)){
                        throw new NullPointerException();
                    }
                    if(password.isEmpty()||password.equalsIgnoreCase("")||password.equalsIgnoreCase(null)){
                        throw new NullPointerException();
                    }
                    if(rePassword.isEmpty()||rePassword.equalsIgnoreCase("")||rePassword.equalsIgnoreCase(null)){
                        throw new NullPointerException();
                    }

                    if(password.equals(rePassword)){
                        convertedPassword = Conversion.convertStringToSHA1AndMD5(password);
                        new CallAddUserAPI().execute();
                    }else{
                        Toast.makeText(getApplicationContext(), "Password do not match.", Toast.LENGTH_SHORT).show();
                    }
                }catch (NullPointerException npe){
                    Toast.makeText(getApplicationContext(), "Please enter all values.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    class CallAddUserAPI extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {
            String urlString = Constants.URL_STRING + "add_user";
            URL url = null;
            try {
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                StringBuffer paramsBuilder = new StringBuffer();
                paramsBuilder.append("name=");
                paramsBuilder.append(name);
                paramsBuilder.append("&username=");
                paramsBuilder.append(username);
                paramsBuilder.append("&password=");
                paramsBuilder.append(convertedPassword);

                PrintWriter requestWriter = new PrintWriter(httpURLConnection.getOutputStream(), true);
                requestWriter.print(paramsBuilder.toString());
                requestWriter.close();

                if (debug) Log.d(TAG, "Response Code : " + httpURLConnection.getResponseCode());
                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

                String response = Conversion.inputStreamToString(in).toString();
                JSONObject jsonObject = new JSONObject(response);
                result = jsonObject.getBoolean(TAG_RESULT);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.d(TAG, "Result : " + result);
            if(result){
                Toast.makeText(getApplicationContext(), "User added successfully.", Toast.LENGTH_SHORT).show();
                finish();
                Intent resultTrueIntent = new Intent(CreateUserActivity.this, AdminActivity.class);
                startActivity(resultTrueIntent);
            }else{
                Toast.makeText(getApplicationContext(), "User already present/User addition failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
