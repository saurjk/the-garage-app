package xyz.saurjk.thegarage.admin;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import xyz.saurjk.thegarage.R;
import xyz.saurjk.thegarage.util.Constants;
import xyz.saurjk.thegarage.util.Conversion;

public class QueryCarActivity extends AppCompatActivity {

    private static final String TAG = "QueryCarActivity";
    private static final boolean debug = true;

    private static final String TAG_RESULT = "result";
    private static final String TAG_SPOT_NAME = "spot_name";
    private static final String TAG_FLOOR = "floor";
    private static final String TAG_VEHICLE_TYPE = "vehicle_type";

    EditText vehicleIdentifierET;
    Button getInfoButton;
    String vehicleIdentifier;
    RelativeLayout resultRelativeLayout;
    Button backButton;

    String spotName;
    String floor;
    String vehicleType;

    TextView vehicleIdentifierTV;
    TextView parkingSpotTV;
    TextView floorTV;
    TextView vehicleTypeTV;

    int result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query_car);

        vehicleIdentifierET = (EditText) findViewById(R.id.vehicleIdentifierET);
        getInfoButton = (Button) findViewById(R.id.getInfoButton);
        resultRelativeLayout = (RelativeLayout) findViewById(R.id.resultRelativeLayout);
        vehicleIdentifierTV = (TextView) findViewById(R.id.vehicleIdentifierTV);
        parkingSpotTV = (TextView) findViewById(R.id.parkingSpotTV);
        floorTV = (TextView) findViewById(R.id.floorTV);
        vehicleTypeTV = (TextView) findViewById(R.id.vehicleTypeTV);
        backButton = (Button) findViewById(R.id.backButton);

        resultRelativeLayout.setVisibility(View.INVISIBLE);

        getInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    vehicleIdentifier = vehicleIdentifierET.getText().toString();
                    if(vehicleIdentifier.isEmpty()||vehicleIdentifier.equalsIgnoreCase("")||vehicleIdentifier.equalsIgnoreCase(null)){
                        throw new NullPointerException();
                    }
                    if (debug) Log.d(TAG, "Vehicle Identifier : " + vehicleIdentifier);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(vehicleIdentifierET.getWindowToken(), 0);

                    new QueryGarageDetailByVehicleID().execute();
                }catch (NullPointerException npe){
                    Toast.makeText(getApplicationContext(), "Please provide a vehicle identifier/license plate.", Toast.LENGTH_SHORT).show();

                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * Methods to connected Android application with PHP API
     */
    class QueryGarageDetailByVehicleID extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {
            String urlString = Constants.URL_STRING + "query_garage_detail_by_vehicle_identifier";
            URL url = null;
            try {
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);

                StringBuffer paramsBuilder = new StringBuffer();
                paramsBuilder.append("vehicle_identifier=");
                paramsBuilder.append(vehicleIdentifier);

                PrintWriter requestWriter = new PrintWriter(httpURLConnection.getOutputStream(), true);
                requestWriter.print(paramsBuilder.toString());
                requestWriter.close();

                if (debug) Log.d(TAG, "Response Code : " + httpURLConnection.getResponseCode());
                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

                String response = Conversion.inputStreamToString(in).toString();

                JSONObject jsonObject = new JSONObject(response);
                result = jsonObject.getInt(TAG_RESULT);

                if(result == 1){
                    spotName = jsonObject.getString(TAG_SPOT_NAME);
                    floor = jsonObject.getString(TAG_FLOOR);
                    vehicleType = jsonObject.getString(TAG_VEHICLE_TYPE);
                }
            }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            switch (result){
                case 0:
                    resultRelativeLayout.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Vehicle Not Found.", Toast.LENGTH_LONG).show();
                    break;
                case 1:
                    resultRelativeLayout.setVisibility(View.VISIBLE);
                    vehicleIdentifierTV.setText(vehicleIdentifier);
                    parkingSpotTV.setText(spotName);
                    floorTV.setText(floor);
                    vehicleTypeTV.setText(vehicleType);
                    break;
            }
        }
    }
}
