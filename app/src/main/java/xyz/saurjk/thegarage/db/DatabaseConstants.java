package xyz.saurjk.thegarage.db;

/**
 * Created by SauRjk on 1/17/2016.
 */
public class DatabaseConstants {

    public static final String TABLE_PARKING_DATA = "TABLE_PARKING_DATA";

    public static final String COLUMN_ID = "_id";

    public static final String COLUMN_VEHICLE_ID = "COLUMN_VEHICLE_ID";
    public static final String COLUMN_PARKING_SPOT = "COLUMN_PARKING_SPOT";
    public static final String COLUMN_ACTIVE = "COLUMN_ACTIVE";

}
