package xyz.saurjk.thegarage.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import xyz.saurjk.thegarage.dto.ParkingSpotDTO;

/**
 * Created by SauRjk on 1/17/2016.
 */
public class DatabaseDataSource {

    private static final String TAG = "DatabaseDataSource";

    private SQLiteDatabase mDatabase;
    private DatabaseHelper mDatabaseHelper;
    private Context mContext;

    public DatabaseDataSource(Context context) {
        this.mContext = context;
        this.mDatabaseHelper = new DatabaseHelper(mContext);
    }

    public void open() throws SQLException {
        mDatabase = mDatabaseHelper.getWritableDatabase();
    }

    public void close() {
        mDatabase.close();
    }

    public void insertParkingSpotData(ParkingSpotDTO parkingSpotDTO) {
        Log.d(TAG, "Insert Parking Spot Data");
        mDatabase.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(DatabaseConstants.COLUMN_VEHICLE_ID, parkingSpotDTO.getVehicleId());
            values.put(DatabaseConstants.COLUMN_PARKING_SPOT, parkingSpotDTO.getParkingSpot());
            values.put(DatabaseConstants.COLUMN_ACTIVE, parkingSpotDTO.getActive());

            mDatabase.insert(DatabaseConstants.TABLE_PARKING_DATA, null, values);
            mDatabase.setTransactionSuccessful();
        } finally {
            mDatabase.endTransaction();
        }
    }

    public List<ParkingSpotDTO> readParkingSpotData() {
        Log.d(TAG, "readParkingSpotData");

        List<ParkingSpotDTO> parkingSpotDTOs = new ArrayList<ParkingSpotDTO>();
        String selectQuery = "SELECT * FROM " + DatabaseConstants.TABLE_PARKING_DATA + " WHERE " + DatabaseConstants.COLUMN_ACTIVE + " = 1";
        Cursor cursor = mDatabase.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                ParkingSpotDTO parkingSpotDTO = new ParkingSpotDTO();
                parkingSpotDTO.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseConstants.COLUMN_ID))));
                parkingSpotDTO.setVehicleId(cursor.getString(cursor.getColumnIndex(DatabaseConstants.COLUMN_VEHICLE_ID)));
                parkingSpotDTO.setParkingSpot(cursor.getString(cursor.getColumnIndex(DatabaseConstants.COLUMN_PARKING_SPOT)));
                parkingSpotDTO.setActive(Integer.parseInt(cursor.getString(cursor.getColumnIndex(DatabaseConstants.COLUMN_ACTIVE))));
                parkingSpotDTOs.add(parkingSpotDTO);
            } while (cursor.moveToNext());
        }

        return parkingSpotDTOs;
    }

    public void exitGarage(String vehicleId) {
        Log.d(TAG, "Exiting Garage");

        String columnId = "";
        Cursor cursor = mDatabase.query(
                DatabaseConstants.TABLE_PARKING_DATA,
                new String[]{DatabaseConstants.COLUMN_ID},
                DatabaseConstants.COLUMN_VEHICLE_ID + " = ? AND " + DatabaseConstants.COLUMN_ACTIVE + " = ?",
                new String[]{vehicleId, "1"},
                null, null, null
        );
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            columnId = cursor.getString(cursor.getColumnIndex(DatabaseConstants.COLUMN_ID));
        }

        ContentValues values = new ContentValues();
        values.put(DatabaseConstants.COLUMN_ACTIVE, new Integer("0"));

        String where = DatabaseConstants.COLUMN_ID + " = ?";
        mDatabase.update(DatabaseConstants.TABLE_PARKING_DATA, values, where, new String[]{columnId});
    }

}
