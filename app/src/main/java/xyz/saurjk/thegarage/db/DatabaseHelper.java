package xyz.saurjk.thegarage.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by SauRjk on 1/17/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME ="THE_GARAGE_APP_DB";
    private static final int DATABASE_VERSION = 2;

    private static final String CREATE_TABLE_APP = "CREATE TABLE " + DatabaseConstants.TABLE_PARKING_DATA + "(" +
            DatabaseConstants.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DatabaseConstants.COLUMN_VEHICLE_ID + " TEXT, " +
            DatabaseConstants.COLUMN_PARKING_SPOT + " TEXT, " +
            DatabaseConstants.COLUMN_ACTIVE + " INTEGER)";


    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_APP);
        Log.d(TAG, "Database Created.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Database Updated.");
    }
}
