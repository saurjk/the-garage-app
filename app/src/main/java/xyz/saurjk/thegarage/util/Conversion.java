package xyz.saurjk.thegarage.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by SauRjk on 1/16/2016.
 */
public class Conversion {
    public static StringBuilder inputStreamToString(InputStream in){
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

        try {
            while ((rLine = bufferedReader.readLine()) != null){
                answer.append(rLine);
            }
        }catch (IOException e){

        }
        return answer;
    }

    public static String convertStringToSHA1AndMD5(String stringToConvert){
        String sha1HexValue = new String(Hex.encodeHex(DigestUtils.sha1(stringToConvert)));
        String md5Value = new String(Hex.encodeHex(DigestUtils.md5(sha1HexValue)));
        return md5Value;
    }

    public static String byteToString(byte[] bytes){
        StringBuilder stringBuilder = new StringBuilder();
        for(byte b : bytes){
            stringBuilder.append(b);
        }
        return stringBuilder.toString();
    }
}
